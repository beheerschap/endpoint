<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 28-12-2018
 * Time: 14:51
 */

namespace Beheerschap\Endpoint;

use PHPUnit\Framework\TestCase;

class EndpointUnitTest extends TestCase
{

    public function testSetDeprecated()
    {
        $this->assertEquals(Endpoint::isDeprecated(), false);
        Endpoint::setDeprecated(true);
        $this->assertEquals(Endpoint::isDeprecated(), true);
    }

    /**
     * @runInSeparateProcess
     */
    public function testAllowCrossOrigin()
    {
        $this->assertNotContains('Access-Control-Allow-Origin: *', xdebug_get_headers());
        $this->assertNotContains('Access-Control-Allow-Methods: *', xdebug_get_headers());
        Endpoint::allowCrossOrigin();
        $this->assertContains('Access-Control-Allow-Origin: *', xdebug_get_headers());
        $this->assertContains('Access-Control-Allow-Methods: *', xdebug_get_headers());
    }

}
