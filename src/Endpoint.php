<?php
/**
 * Created by PhpStorm.
 * User: beheerschap
 * Date: 18-12-2018
 * Time: 13:05
 */

namespace Beheerschap\Endpoint;

class Endpoint {
    private static $deprecated = false;

    /**
     * @param bool $deprecated
     */
    public static function setDeprecated(bool $deprecated): void
    {
        self::$deprecated = $deprecated;
    }

    /**
     * @return bool
     */
    public static function isDeprecated(): bool
    {
        return self::$deprecated;
    }

    /**
     * @param string $origin
     * @param string $methods
     */
    public static function allowCrossOrigin($origin = '*', $methods = '*')
    {
        header("Access-Control-Allow-Origin: {$origin}");
        header("Access-Control-Allow-Methods: {$methods}");
    }

}
